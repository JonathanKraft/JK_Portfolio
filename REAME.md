Jonathan Kraft's Portfolio

Hello and welcome.
You will find in theses files my portfolio. It may seem quite empty but it will fill up over time !

Getting Started

You can git clone my project and open it in you browser if you'd like.

Libraries used
    Google Font - Free to use fonts
    FontAwesome - Uses font to represent logos

Versioning

I used git for versioning. For the versions available, see the tags on this repository.

Authors

    Jonathan KRAFT

Thanks for reading !
